import  React from 'react';
import  AzsService  from  './components/AzsService';

import { MapContainer, TileLayer  } from 'react-leaflet';
import {MyMarker, DopInfo} from './components/MyMarker'

import './css/BaseCss.css';
import "../node_modules/leaflet/dist/leaflet.css";

export const AzsContext = React.createContext({});

const azsService = new AzsService();
const center = [55, 49]
const zoom = 7

const AzsList = () => {
  const [map, setMap] = React.useState(null)
  const [stations, setStatons] = React.useState([]);
  const [selectedAzs, setSelectedAzs] = React.useState(null)

  React.useEffect(()=> {
    if (map) {
        const extremePoints = map.getBounds();
    const northEast = extremePoints["_northEast"];
    const southWest = extremePoints["_southWest"];

    azsService.getStations(northEast.lat, northEast.lng, southWest.lat, southWest.lng).then((result) => {setStatons(result.data)})
    }
    
    },[map]);

  const onMove = React.useCallback(() => {
    if (map) {
        const extremePoints = map.getBounds();
        const northEast = extremePoints["_northEast"];
        const southWest = extremePoints["_southWest"];

        azsService.getStations(northEast.lat, northEast.lng, southWest.lat, southWest.lng).then(
        (result) => {setStatons(result.data)})
    }

  }, [map]);

    React.useEffect(() => {
        if(map){
            map.on('move', onMove)
            return () => map.off('move', onMove);
        }
    }, [map, onMove]);
  

  return (
    <AzsContext.Provider value={{selectedAzs, setSelectedAzs}}>
            <div>
                {selectedAzs ? <DopInfo/> : <div className='dopinfo'>Select Azs<hr /></div>}
                <MapContainer
                    center={center}
                    zoom={zoom}
                    scrollWheelZoom={true}
                    ref={setMap}
                >
                        {/* <Markers/> */}
                        {stations.map((station, index) => <MyMarker key={`marker${index}`} currStation={station} />)}
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                </MapContainer>
            </div>
        </AzsContext.Provider>
  )
}
export  default  AzsList;
