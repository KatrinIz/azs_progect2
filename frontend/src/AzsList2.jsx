import  React from 'react';
import  AzsService  from  './components/AzsService';


import * as L from 'leaflet';


import { MapContainer, TileLayer  } from 'react-leaflet';

import "./LeafletVectorGridbundled/Leaflet.VectorGrid.bundled";
import "./LeafletVectorGridbundled/Leaflet.VectorGrid";


import './css/BaseCss.css';
import "../node_modules/leaflet/dist/leaflet.css";


export const AzsContext = React.createContext({});

const azsService = new AzsService();
const center = [55, 49]
const zoom = 7
// ============================
// https://github.com/Leaflet/Leaflet.VectorGrid
// npm install --save lodash
// npm install leaflet.vectorgrid

//  -документация https://leaflet.github.io/Leaflet.VectorGrid/vectorgrid-api-docs.html
// Get yours KEY at https://maptiler.com/cloud/
// ============================

const AzsList = () => {
  // const [map, setMap] = React.useState(null)
  // const [stations, setStatons] = React.useState([]);
  // const [selectedAzs, setSelectedAzs] = React.useState(null)

  // React.useEffect(()=> {
  //   if (map) {
  //       const extremePoints = map.getBounds();
  //   const northEast = extremePoints["_northEast"];
  //   const southWest = extremePoints["_southWest"];

  //   azsService.getStations(northEast.lat, northEast.lng, southWest.lat, southWest.lng).then((result) => {setStatons(result.data)})
  //   }
    
  //   },[map]);

  // const onMove = React.useCallback(() => {
  //   if (map) {
  //       const extremePoints = map.getBounds();
  //       const northEast = extremePoints["_northEast"];
  //       const southWest = extremePoints["_southWest"];

  //       azsService.getStations(northEast.lat, northEast.lng, southWest.lat, southWest.lng).then(
  //       (result) => {setStatons(result.data)})
  //   }

  // }, [map]);

    // React.useEffect(() => {
    //     if(map){
    //         map.on('move', onMove)
    //         return () => map.off('move', onMove);
    //     }
    // }, [map, onMove]);
  const openmaptilesUrl = "http://localhost:8000/map/{z}/{x}/{y}.pbf";
  // const openmaptilesUrl = "https://api.maptiler.com/tiles/v3/{z}/{x}/{y}.pbf?key={key}";
  
  // const vectorStyles = {
  //   water: {	// Apply these options to the "water" layer...
  //     fill: true,
  //     weight: 1,
  //     fillColor: '#06cccc',
  //     color: '#06cccc',
  //     fillOpacity: 0.2,
  //     opacity: 0.4,
  //   },
  //   transportation: {	// Apply these options to the "transportation" layer...
  //     weight: 0.5,
  //     color: '#f2b648',
  //     fillOpacity: 0.2,
  //     opacity: 0.4,
  //   },
  // };

  const vectorStyles = {width: "100%", height: "100vh"};
    
  React.useEffect(() => {var openMapTilesLayer = L.vectorGrid.protobuf(openmaptilesUrl, {
      vectorTileLayerStyles: {width: "100%", height: "100vh"},
      subdomains: '0123',
      attribution: '© OpenStreetMap contributors, © MapTiler',
      //  key: 'abcdefghi01234567890' // Get yours at https://maptiler.com/cloud/
       key: '5QVEl0hEhJEZQLYwPScE'
    });

    var container = L.DomUtil.get('map');
      if(container != null){
        container._leaflet_id = null;
      }

    var map = new L.map('map');
    map.setView(new L.LatLng(55, 49), 5 );
    map.options.minZoom = 2;
    map.options.maxZoom = 7;
    openMapTilesLayer.addTo(map);}, []);

  return (

    <div>

      <div id="map"></div>

    </div>

    // <AzsContext.Provider value={{selectedAzs, setSelectedAzs}}>
    //         <div>
    //             {selectedAzs ? <DopInfo/> : <div className='dopinfo'>Select Azs<hr /></div>}
    //             <MapContainer
    //                 center={center}
    //                 zoom={zoom}
    //                 scrollWheelZoom={true}
    //                 ref={setMap}
    //             >
    //                     {/* <Markers/> */}
    //                     {stations.map((station, index) => <MyMarker key={`marker${index}`} currStation={station} />)}
    //                 <TileLayer
    //                     attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    //                     url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    //                 />
    //             </MapContainer>
    //         </div>
    //     </AzsContext.Provider>
  )
}
export  default  AzsList;
