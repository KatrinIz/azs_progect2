import AzsList from './AzsListOnline';
import './App.css';

function App() {
  return (
    <div className="App">
        <AzsList />
    </div>
  );
}

export default App;
