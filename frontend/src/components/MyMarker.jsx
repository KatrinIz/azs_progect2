import  React from 'react';
import { Marker, Popup, useMap } from 'react-leaflet';
import L from "leaflet";
import gp_icon from '.././imgs/gp.svg';
import tat_icon from '.././imgs/tat.svg';
import  AzsService  from  '../components/AzsService';
import { AzsContext } from '../AzsListOnline';

const azsService = new AzsService();

const gpIcon = new L.Icon({
    iconUrl: gp_icon,
    iconSize: new L.Point(35, 35),
  });
const tatIcon = new L.Icon({
    iconUrl: tat_icon,
    iconSize: new L.Point(30, 30),
  });

const dicContecst = {
    'tatneft': 'TATneft',
    'gazprom': 'GazProm',
  };

const dicIcons = {
    'tatneft': tat_icon,
    'gazprom': gp_icon,
  };

export const DopInfo = () => {
  const {selectedAzs} = React.useContext(AzsContext);
  const {setSelectedAzs} = React.useContext(AzsContext)
  const { stations } = React.useContext(AzsContext);
  const { setCenter } = React.useContext(AzsContext);

  
  const [info, setInfo] = React.useState(null);

  React.useEffect(() => {
    if (selectedAzs) {
        azsService.getInformStationById(selectedAzs.id).then((result) => {
            setInfo(result);
        })
    }
  }, [selectedAzs]);
 
  return (
    <div className='dopinfo'>
        {
            (info) ?
                <div className='dopinfo_content'>
                    <div className='title'>
                        <img 
                            src={dicIcons[selectedAzs.azstype__name]}
                            width="40" height="40" alt="image description" className='image'
                        /> 
                        <b>{dicContecst[selectedAzs.azstype__name]}</b> 
                    </div>
                    <hr /><br/>
                    {info.dop_inf.address}<br/>
                    <table className='pricetable'>
                        <thead >
                            <tr height="28px" >
                                <th width="125px" border="solid white"> марка </th>
                                <th width="125px"> цена  </th>
                        </tr>
                        </thead>
                        <tbody>
                            {info.benzines.map((fuel, index) => <tr key={`row${index}`}>
                                <td key={`fuel${index}`}>{fuel.benzine__name}</td>
                                <td key={`cost${index}`}>{fuel.cost}</td></tr>
                            )}
                        </tbody>
                    </table>
                    <hr/>
                    <div className='contentFuter'>
                        <b>Услуги:</b> {info.dop_inf.services}
                    </div>                   
                </div> :
                <div className='dopinfo_content'> Select Azs </div>
        }
        
        <div className='emptydiv'>
            {stations.map((station, index) => 
                <div key={`azs${index}`} className='listAzs' onClick={(e) => 
                  {
                    e.stopPropagation();
                    e.preventDefault();
                    setSelectedAzs(station);
                    setCenter([station.lat, station.lon]);
                  }}
                >
                    <img 
                        src={dicIcons[station.azstype__name]}
                        width="25" height="25" alt="image description" className='image'
                    /> 
                    <span>{`№${station.number}`}</span>
                    <span className='address'>{station.address}</span>    
                </div>
            )}
        </div>
        
    </div>
  )
}

export const MyMarker = (props) => {
    const {setSelectedAzs} = React.useContext(AzsContext)
    const {currStation} = props;
    return (
      <Marker position={[currStation.lat, currStation.lon]}
        icon={currStation.azstype__name=== 'gazprom' ? gpIcon: tatIcon}
        eventHandlers={{
          click: () => setSelectedAzs(currStation),
        }}>
        <Popup>
          This station number: {currStation.number}
          {/* <StationInfo id={currStation.id}/> */}
        </Popup>
      </Marker>
    )
};
