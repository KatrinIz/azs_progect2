# syntax=docker/dockerfile:1
FROM ubuntu:20.04
RUN apt-get update
RUN apt install -y python3-pip curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_16.x  | bash -
RUN apt-get -y install nodejs

COPY . /app
WORKDIR /app/frontend
RUN npm install
RUN npm run build

WORKDIR /app
RUN pip install -r requirements.txt
RUN python3 manage.py migrate
RUN python3 manage.py start_bd
CMD python3 /app/manage.py runserver
